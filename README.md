# auto-print.terradeo

## Why would I need that?

Use this if you want to be only one click away selecting a fil to print at @Terradeo.

## Installation

### Libraries

```
pip install selenium
pip install pyinstaller
```

### Vitual environment
```
virtualenv -p \Path\To\Python37\python.exe .venv
```

Then, each time you want to run the program,

```
.\.venv\Scripts\activate
```

### Drivers
Download latest chromedriver : http://chromedriver.chromium.org/downloads

Extract the .exe inside a folder.

Add this folder to the Path variable.

## Config file

```
DON'T SEND ME YOUR PASSWORD
(Never push/share your config.ini file. You have been warned)
```

Set your username (LDAP) and password in the `config.ini` file. Don't use brackets.

To ignore this file, use this (with git installed) :

```
git update-index --skip-worktree config.ini
```
(Courtesy of [Ian Gloude](https://medium.com/@igloude/git-skip-worktree-and-how-i-used-to-hate-config-files-e84a44a8c859))

## Generate the .exe

Create spec file

```
pyi-makespec --onefile  --name auto-print.terradeo .\main.py
```
Then, at the end of the created file `.\auto-print.terradeo.spec`, add the following :

```
import shutil
shutil.copyfile('config.ini', '{0}/config.ini'.format(DISTPATH))
```

(Courtesy of [Stefano Giraldi](https://stackoverflow.com/a/47886145))

Finally, generate the .exe

```
pyinstaller --clean .\auto-print.terradeo.spec
```

Check your `./dist/` folder, you can see the config.ini.

# There you go

`Right click on exe file > Add to desktop `
