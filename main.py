from selenium import webdriver
import configparser

# Get credentials and stuff
config = configparser.ConfigParser()
config.read('config.ini')
# Open webdriver
browser = webdriver.Chrome()
# Browse to OBMS and set credentials
browser.get(config['DEFAULT']['url'])
login = browser.find_element_by_name('Username')
password = browser.find_element_by_name('Password')
login.send_keys(config['DEFAULT']['login'])
password.send_keys(config['DEFAULT']['password'])
# Log in OBMS
browser.find_element_by_class_name('buttonwhiteongreysmall').click()
# Check if connected
if browser.title == "EveryonePrint - Mes Impressions" :
    # Open fil explorer
    browser.find_element_by_name('FileToPrint').click()
